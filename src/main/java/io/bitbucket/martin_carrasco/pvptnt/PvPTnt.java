/*
 * Copyright (C) 2016 Martin Carrasco <martin-1998 at hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.bitbucket.martin_carrasco.pvptnt;

import org.bukkit.craftbukkit.v1_9_R1.CraftServer;
import org.bukkit.plugin.java.JavaPlugin;

public class PvPTnt extends JavaPlugin
{
    private static PvPTnt instance;
    @Override
    public void onEnable()
    {
        instance = this;
        ((CraftServer)this.getServer()).getCommandMap().register("customtnt", new CMD_tnt());
        this.getServer().getPluginManager().registerEvents(new tListener(), this);
    }
    public static PvPTnt getInstance(){
        return instance;
    }
}
