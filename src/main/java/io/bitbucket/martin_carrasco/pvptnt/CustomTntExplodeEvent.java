/*
 * Copyright (C) 2016 Martin Carrasco <martin-1998 at hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.bitbucket.martin_carrasco.pvptnt;

import java.util.Random;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class CustomTntExplodeEvent extends Event implements Cancellable
{
    private boolean cancelled = false;
    private TntType type;
    private ProjectileHitEvent e;
    public static final HandlerList handlers = new HandlerList();
    
    public CustomTntExplodeEvent(TntType type, ProjectileHitEvent e)
    {
        this.type = type;
        this.e = e;
        
        Location loc = e.getEntity().getLocation();
        Player p = (Player) e.getEntity().getShooter();
        e.getEntity().remove();
        switch(type)
        {
            case DEFAULT:
                loc.getWorld().createExplosion(loc, 4F);
                break;
            case POISON:
                loc.getWorld().createExplosion(loc, 2F);
                for(Entity ent : loc.getWorld().getNearbyEntities(loc, 5, 5, 5))
                {
                    if(ent instanceof LivingEntity){
                        ((LivingEntity)ent).addPotionEffect(new PotionEffect(PotionEffectType.POISON, 100, 2));
                    }
                }
                break;
            case FRAG:
                Random r = new Random();
                int count = r.nextInt(5-3)+3;
                loc.getWorld().createExplosion(loc, 2F);
                for(int x = loc.getBlockX() - 5;x < loc.getBlockX()+5;x++)
                {
                    if(count == 0)
                        break;
                    for(int z = loc.getBlockZ()-5;z < loc.getBlockZ()+5;z++)
                    {
                        if(r.nextInt(4) == 1){
                            Location newLoc = new Location(loc.getWorld(), x, loc.getBlockY(), z);
                            loc.getWorld().spawnEntity(newLoc, EntityType.PRIMED_TNT);
                            count--;
                        }
                            
                    }
                }
                break;
            case DAMAGE:
                Random ra = new Random();
                loc.getWorld().createExplosion(loc, 2F);
                for(Entity ent : loc.getWorld().getNearbyEntities(loc, 5, 5, 5))
                {
                    if(ent instanceof LivingEntity){
                        ((LivingEntity) ent).damage(ra.nextInt(6), p);
                    }
                }
                break;
            case MOB:
                int countB = 5;
                Random ran = new Random();
                loc.getWorld().createExplosion(loc, 2F);
                for(int x = loc.getBlockX() - 5;x < loc.getBlockX()+5;x++)
                {
                    if(countB == 0)
                        break;
                    for(int z = loc.getBlockZ()-5;z < loc.getBlockZ()+5;z++)
                    {
                        if(ran.nextInt(4) == 1){
                            Location newLoc = new Location(loc.getWorld(), x, loc.getBlockY(), z);
                            loc.getWorld().spawnEntity(newLoc, getNextEnt());
                            countB--;
                        }
                            
                    }
                }
                break;
        }
    }
    
    public TntType getType(){
        return type;
    }
    public ProjectileHitEvent getEvent(){
        return e;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }
    private EntityType getNextEnt(){
        Random r = new Random();
        return r.nextInt(4) == 1 ? EntityType.CREEPER : 
               r.nextInt(4) == 1 ? EntityType.ZOMBIE : 
               r.nextInt(4) == 1 ? EntityType.BLAZE : 
               r.nextInt(4) == 1 ? EntityType.SKELETON :
               r.nextInt(4) == 1 ? EntityType.WITCH :
               r.nextInt(10) == 1  ? EntityType.WITHER : EntityType.CHICKEN;
    }
}
