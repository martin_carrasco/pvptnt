/*
 * Copyright (C) 2016 Martin Carrasco <martin-1998 at hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.bitbucket.martin_carrasco.pvptnt;

import net.minecraft.server.v1_9_R1.Entity;
import org.bukkit.craftbukkit.v1_9_R1.CraftServer;
import org.bukkit.craftbukkit.v1_9_R1.entity.CraftProjectile;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Projectile;
import org.bukkit.projectiles.ProjectileSource;

public class CustomTnT extends CraftProjectile implements Projectile
{

    public CustomTnT(CraftServer server, Entity entity) {
        super(server, entity);
    }

    @Override
    public EntityType getType() {
        return EntityType.UNKNOWN;
    }

    @Override
    public void setInvulnerable(boolean flag) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isInvulnerable() {
        return false;
    }

    @Override
    public LivingEntity _INVALID_getShooter() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ProjectileSource getShooter() {
        return this.entity.projectileSource;
    }

    @Override
    public void _INVALID_setShooter(LivingEntity shooter) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setShooter(ProjectileSource source) {
        this.entity.projectileSource = source;
    }

}
