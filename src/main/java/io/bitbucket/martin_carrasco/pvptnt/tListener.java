/*
 * Copyright (C) 2016 Martin Carrasco <martin-1998 at hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.bitbucket.martin_carrasco.pvptnt;

import java.util.HashMap;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class tListener implements Listener
{
    public static final HashMap<Integer, TntType> customTnt = new HashMap<>();
    @EventHandler
    public void onHit(ProjectileHitEvent e)
    {
        if(e.getEntity() instanceof CustomTnT
                && customTnt.containsKey(e.getEntity().getEntityId()))
        {
            PvPTnt.getInstance().getServer().getPluginManager().callEvent(new CustomTntExplodeEvent(customTnt.get(e.getEntity().getEntityId()), e));
        }
    }
    @EventHandler
    public void onClick(PlayerInteractEvent e)
    {
        if(e.getAction() == Action.RIGHT_CLICK_AIR
               && isCustomTnt(e.getItem()))
        {
            Player p = e.getPlayer();
            if(!p.hasPermission("pvptnt."+ChatColor.stripColor(e.getItem().getItemMeta().getDisplayName()).split(" ")[0])){
                p.sendMessage(ChatColor.RED + "You don't have permission to use this tnt");
                return;
            }
            Entity ent = p.launchProjectile(CustomTnT.class, p.getLocation().getDirection().multiply(2));
            ent.setFireTicks(ent.getMaxFireTicks());
            customTnt.put(ent.getEntityId(), TntType.getByName(ChatColor.stripColor(e.getItem().getItemMeta().getDisplayName())));
            p.getInventory().setItemInMainHand(null);
            e.setCancelled(true);
        }
    }
    private boolean isCustomTnt(ItemStack i)
    {
        return i != null
                && i.getType() == Material.TNT 
                && i.hasItemMeta() 
                && i.getItemMeta().hasDisplayName() 
                && TntType.getByName(ChatColor.stripColor(i.getItemMeta().getDisplayName())) != null;
        
    }
}
