/*
 * Copyright (C) 2016 Martin Carrasco <martin-1998 at hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.bitbucket.martin_carrasco.pvptnt;

import java.util.List;

/**
 *
 * @author Martin Carrasco <martin-1998 at hotmail.com>
 */
public enum TntType
{
    DEFAULT("Default TnT"),
    POISON("Poison TnT"),
    FRAG("Fragmentation TnT"),
    DAMAGE("Damage TnT"),
    MOB("Monster TnT");
    
    private String name;
    TntType(String name)
    {
        this.name = name;
    }
    public String getName(){
        return name;
    }
    public static TntType getByName(String name)
    {
        switch(name.toLowerCase())
        {
            case "fragmentation tnt":
                return FRAG;
            case "default tnt":
                return DEFAULT;
            case "damage tnt":
                return DAMAGE;
            case "monster tnt":
                return MOB;
            case "poison tnt":
                return POISON;
            default:
                return null;
        }
    }
}
