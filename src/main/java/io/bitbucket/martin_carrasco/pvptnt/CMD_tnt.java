/*
 * Copyright (C) 2016 Martin Carrasco <martin-1998 at hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.bitbucket.martin_carrasco.pvptnt;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CMD_tnt extends Command
{
    public CMD_tnt(){
        super("ctnt");
    }
    @Override
    public boolean execute(CommandSender cs, String commandLabel, String[] args) 
    {
        if(!cs.hasPermission("pvptnt.give")){
            cs.sendMessage(ChatColor.RED + "You don't have permission to do this");
            return false;
        }
        if(args.length < 3){
            cs.sendMessage(ChatColor.RED + "Usage: /ctnt <player> <type> <amount>");
            return false;
        }
        if(PvPTnt.getInstance().getServer().getPlayer(args[0]) == null){
            cs.sendMessage(ChatColor.RED + "Player not found");
            return false;
        }
        if(TntType.getByName(args[1] + " tnt") == null){
            cs.sendMessage(ChatColor.RED + "Not an accepted tnt type");
            return false;
        }
        
        Player p = PvPTnt.getInstance().getServer().getPlayer(args[0]);
        TntType type = TntType.getByName(args[1] + " tnt");
        int amount = Integer.parseInt(args[2]);
        
        ItemStack i = new ItemStack(Material.TNT, amount);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(ChatColor.LIGHT_PURPLE + type.getName());
        i.setItemMeta(im);
        
        p.getInventory().addItem(i);
        
        p.sendMessage(ChatColor.GREEN + "You received "+ ChatColor.RED + amount + ChatColor.GREEN + type.getName());
        cs.sendMessage(ChatColor.GREEN + "Gave player desire TNT");
        return true;
    }
}
